'use strict';
var expect = require('chai').expect
var utils = require('../lib/utils')

describe('Util Functions', function () {
  it('should have correct probibilities', function (done) {
    let options = {
      like: .9,
      comment: .15,
      follow: .15
    }
    let total = 1000;
    let allProbsChosen = {};
    let probs = utils.getProbsFromOptions(options);
    for (var probKey in probs) {
      for (var i of new Array(total).fill(0)) {
        let chosen = utils.randomWithProbability(probs[probKey]);
        if (!allProbsChosen[chosen]) allProbsChosen[chosen] = [];
        allProbsChosen[chosen].push(chosen);
      }
    }
    expect(allProbsChosen['like'].length / total).to.be.closeTo(options.like, .05);
    expect(allProbsChosen['notLike'].length / total).to.be.closeTo(1 - options.like, .05);
    expect(allProbsChosen['comment'].length / total).to.be.closeTo(options.comment, .05);
    expect(allProbsChosen['notComment'].length / total).to.be.closeTo(1 - options.comment, .05);
    expect(allProbsChosen['follow'].length / total).to.be.closeTo(options.follow, .05);
    expect(allProbsChosen['notFollow'].length / total).to.be.closeTo(1 - options.follow, .05);
    done();
  })
})