'use strict';
var expect = require('chai').expect
var { Instagram } = require('../');
var fs = require('fs-extra');
var utils = require('../lib/utils')
var nocks = require('./nocks/nockMain');
// var nock = require('nock')
// nock.recorder.rec({
//   use_separator: false
// });
let useMock = !process.env.npm_config_live;
if (useMock) console.log('Using nock for API requests')
else console.log('Using live instagram API requests')
describe('Instagram API Login Functions', function () {
  beforeEach(async () => {
    return await utils.nonBlockingSleep(.1);
  })
  it('should login with correct credentials, if not already logged in', async function () {
    this.timeout(20000)
    if (useMock) nocks[0]();
    var api = new Instagram('dizuci', 'Donkeyy1', true);
    let result = await api.login(true);
    expect(result).to.be.true;
  });
  it('should login with correct credentials, if already logged in', async function () {
    this.timeout(20000)
    if (useMock) nocks[1]();
    var api = new Instagram('dizuci', 'Donkeyy1', true);
    let result = await api.login();
    expect(result).to.be.true;
  });
  it('should login with incorrect credentials if already logged in', async function () {
    this.timeout(20000)
    if (useMock) nocks[2]()
    var api = new Instagram('dizuci', 'random_password', true);
    let result = await api.login();
    expect(result).to.be.true;
  });
  it('should not login with incorrect credentials if not already logged in', async function () {
    this.timeout(20000)
    if (useMock) nocks[3]('dgfhjkj', 'random_password')
    var api = new Instagram('dgfhjkj', 'random_password', true);
    let result = await api.login(true);
    expect(result).to.be.false;
  });
})

var api;
describe('Instagram API Follow Functions', function () {
  before(async function () {
    this.timeout(20000)
    if (useMock) nocks[0]()
    api = new Instagram('dizuci', 'Donkeyy1', true);
    return await api.login();
  });
  it('should be able to follow an account', async function () {
    this.timeout(20000)
    if (useMock) nocks[4](25025320)
    let { result } = await api.follow(25025320, 'instagram');
    expect(result === 'following').to.be.true;
  });
  it('should be able to unfollow an account', async function () {
    this.timeout(20000)
    if (useMock) nocks[5](25025320)
    let { result } = await api.unfollow(25025320, 'instagram');
    expect(result === 'following').to.be.false;
  });
})

describe('Instagram API Like Functions', function () {
  it('should be able to like a post', async function () {
    this.timeout(20000)
    if (useMock) nocks[6]("1633012024086507521")
    let result = await api.like("1633012024086507521", 'instagramjapan');
    expect(result.status === 'ok').to.be.true;
  });
  it('should be able to unlike a post', async function () {
    this.timeout(20000)
    if (useMock) nocks[7]("1633012024086507521")
    let result = await api.unlike("1633012024086507521", 'instagramjapan');
    expect(result.status === 'ok').to.be.true;
  });
})

describe('Instagram API Comment Functions', function () {
  var commentID;
  let postID = "1633617620229172382";
  it('should be able to comment on a post', async function () {
    this.timeout(30000)
    if (useMock) nocks[8](postID)
    let randomEmoji = await utils.getRandomEmoji();
    let result = await api.comment(postID, 'BarxpIAlCye', randomEmoji, 'therock');
    expect(result.status === 'ok').to.be.true;
    commentID = result.id;
  });
  it('should be able to delete a comment on a post', async function () {
    this.timeout(30000)
    if (useMock) nocks[9](postID, commentID)
    let result = await api.deleteComment(postID, 'BarxpIAlCye', commentID, 'therock');
    expect(result.status === 'ok').to.be.true;
  });
})