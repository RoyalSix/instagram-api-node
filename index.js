// Initialize Firebase
var firebase = require('firebase');
var config = {
  apiKey: "AIzaSyCqzVE4sU4rpUgKkYOdhEFaOkOb3yBdZ_c",
  authDomain: "instagram-api-node.firebaseapp.com",
  databaseURL: "https://instagram-api-node.firebaseio.com",
  projectId: "instagram-api-node",
  storageBucket: "instagram-api-node.appspot.com",
  messagingSenderId: "529946407263"
};
module.exports.firebase = firebase.initializeApp(config, 'instagram-api-node');

module.exports.Instagram = require('./lib/instagram.js');
module.exports.Bot = require('./lib/bot/');

// var api = new this.Instagram('petsoftheday_ig', "Thehundreds");
// var self = this;
// (async function () {
//   await api.login(true);
//   var bot = new self.Bot(api).start()
// })();


